package com.city.bangalore.controller;

import com.city.bangalore.constants.Lights;
import com.city.bangalore.signals.ECSignal;
import com.city.bangalore.signals.SilkBoardSignal;

public class BangaloreController {

	public static void main(String[] args) {
		SilkBoardSignal silkBoard = new SilkBoardSignal();
		System.out.println("SilkBoard Signal Details");
		silkBoard.getGreenSignalTime();
		silkBoard.display(Lights.GREEN);
		silkBoard.display(Lights.YELLOW);
		silkBoard.display(Lights.RED);
		System.out.println();
		ECSignal ecSignal = new ECSignal();
		System.out.println("ECSignal details");
		ecSignal.getGreenSignalTime();
		ecSignal.display(Lights.GREEN);
		ecSignal.display(Lights.YELLOW);
		ecSignal.display(Lights.RED);

	}

}
