package com.city.bangalore.signals;

import com.city.bangalore.constants.Lights;

abstract public class BaseSignal {
	protected int yellowSignalSec = 5;
	protected int redSignalSec = 10;
	protected int greenSignalSec;

	abstract protected int getGreenSignal();

	public int getGreenSignalTime() {
		greenSignalSec = getGreenSignal();
		return greenSignalSec;
	}

	public void display(Lights light) {
		System.out.println("Glown light: " + light + " for " + getSignalTiming(light) + " Seconds");
	}

	public int getSignalTiming(Lights light) {
		int signalTime = 0;
		switch (light) {
		case GREEN:
			signalTime = greenSignalSec;
			break;
		case YELLOW:
			signalTime = redSignalSec;
			break;
		case RED:
			signalTime = yellowSignalSec;
			break;
		default:
			System.out.println("Invalid light color");
			break;
		}
		return signalTime;

	}
}
